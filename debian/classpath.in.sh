ES_CLASSPATH=$ES_CLASSPATH:$( find /usr/share/java \
    -name 'elasticsearch-1.7.*.jar' \
    -or -name 'sigar.jar' \
    -or -name 'jna.jar' \
    -or -name 'jts.jar' \
    -or -name 'log4j-1.2-*.jar' \
    -or -name 'apache-log4j-extras-1.2.*.jar' \
    -or -name 'lucene-*-4.10.*.jar' \
    -or -name 'spatial4j-0.4*.jar' \
    -or -name 'groovy-all.jar' \
    | tr '\n' ':' | sed -e 's,::*,:,g' )
