Source: elasticsearch
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Hilko Bengen <bengen@debian.org>
Section: database
Build-Depends-Indep: debhelper (>= 9), maven, maven-debian-helper (>= 1.6), default-jdk,
 groovy (>= 2.4),
 libcarrotsearch-hppc-java,
 libcommons-lang3-java,
 libcompress-lzf-java,
 libguava-java (>= 18),
 libhyperic-sigar-java,
 libjackson2-annotations-java,
 libjackson2-core-java,
 libjackson2-databind-java,
 libjackson2-dataformat-cbor,
 libjackson2-dataformat-smile,
 libjackson2-dataformat-yaml,
 libjna-java,
 libjoda-convert-java,
 libjoda-time-java,
 libjts-java (>= 1.13),
 liblog4j1.2-java,
 liblog4j-extras1.2-java,
 liblucene4.10-java,
 libmaven-antrun-plugin-java,
 libmaven-install-plugin-java,
 libmaven-shade-plugin-java,
 libnetty-3.9-java,
 libslf4j-java,
 libspatial4j-0.4-java,
 libt-digest-java,
 libmustache-java,
Priority: optional
Standards-Version: 3.9.8
Vcs-Git: https://anonscm.debian.org/git/pkg-java/elasticsearch.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-java/elasticsearch.git
Homepage: http://www.elasticsearch.org/

Package: libelasticsearch1.7-java
Architecture: all
Section: java
Depends: ${misc:Depends},
 groovy (>= 2.4),
 libhyperic-sigar-java,
 libjna-java,
 libjts-java (>= 1.13),
 liblog4j1.2-java,
 liblog4j-extras1.2-java,
 liblucene4.10-java,
 libspatial4j-0.4-java,
Breaks: elasticsearch (<< 1.0.3+dfsg-6~)
Replaces: elasticsearch (<< 1.0.3+dfsg-6~)
Built-Using: ${misc:Built-Using}
Description: Open Source, Distributed, RESTful Search Engine -- libraries
 Elasticsearch is a search server based on Lucene. It provides a
 distributed, multitenant-capable full-text search engine with a
 RESTful web interface and schema-free JSON documents.
 .
 This package contains the .jar file and everything that is needed to
 build extensions.

Package: elasticsearch
Architecture: all
Depends: ${misc:Depends}, adduser,
 default-jre-headless | java6-runtime-headless,
 libelasticsearch1.7-java,
 java-wrappers,
Description: Open Source, Distributed, RESTful Search Engine
 Elasticsearch is a search server based on Lucene. It provides a
 distributed, multitenant-capable full-text search engine with a
 RESTful web interface and schema-free JSON documents.
 .
 This package contains the infrastructure needed to an ElasticSearch node.
